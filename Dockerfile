# syntax=docker/dockerfile

FROM python:3.10
WORKDIR /sem_lABINTSEV_19_05
COPY . .
RUN pip install -r requirements.txt
CMD ["python", "src/predict_app.py"]
EXPOSE 5000