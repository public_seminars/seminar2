#!/usr/bin/env python
#-*- coding: utf-8 -*-
import os

import boto3
import click
from dotenv import dotenv_values, load_dotenv


# config = dotenv_values(".env")
# aws_access_key_id = config['aws_access_key_id']
# aws_secret_access_key = config['aws_secret_access_key']

load_dotenv('.env')


@click.command()
@click.option('--model_path', default='models/model_1.joblib')
@click.option('--bucket_name', default='pabdcv-3m')
@click.option('--s3_model_name', default='model_221712.joblib')
def upload_model_s3(model_path, bucket_name, s3_model_name):
    # aws_access_key_id = os.getenv('aws_access_key_id')
    # aws_secret_access_key = os.getenv('aws_secret_access_key')


    session = boto3.session.Session()
    s3 = session.client(
        service_name='s3',
        endpoint_url='https://storage.yandexcloud.net'
        # aws_access_key_id=aws_access_key_id,
        # aws_secret_access_key=aws_secret_access_key
    )

    # Создать новый бакет
    # s3.create_bucket(Bucket=bucket_name)
    #
    # # Загрузить объекты в бакет
    #
    # ## Из строки
    # s3.put_object(Bucket=bucket_name, Key='object_name', Body='TEST', StorageClass='COLD')
    #
    ## Из файла
    s3.upload_file(model_path, bucket_name, s3_model_name)
    # s3.upload_file('this_script.py', 'bucket-name', 'script/py_script.py')

    # Получить список объектов в бакете
    for key in s3.list_objects(Bucket=bucket_name)['Contents']:
        print(key['Key'])

    # # Удалить несколько объектов
    # forDeletion = [{'Key':'object_name'}, {'Key':'script/py_script.py'}]
    # response = s3.delete_objects(Bucket='bucket-name', Delete={'Objects': forDeletion})
    #
    # # Получить объект
    # get_object_response = s3.get_object(Bucket='bucket-name',Key='py_script.py')
    # print(get_object_response['Body'].read())


if __name__ == '__main__':
    upload_model_s3()